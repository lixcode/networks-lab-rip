#ifndef SERVER_H
#define SERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>

#include <qhttpserver.hpp>
#include <qhttpserverrequest.hpp>
#include <qhttpserverresponse.hpp>

#include "neighbor.h"

class Server : public QObject
{
	Q_OBJECT

	Q_PROPERTY (QString address READ address WRITE setAddress NOTIFY addressChanged)
	Q_PROPERTY (QHostAddress ip READ ip WRITE setIp NOTIFY ipChanged)
	Q_PROPERTY (QHostAddress mask READ mask WRITE setMask NOTIFY maskChanged)
	Q_PROPERTY (QHostAddress gateway READ gateway WRITE setGateway NOTIFY gatewayChanged)
	Q_PROPERTY (QList <Neighbor *> table READ table WRITE setTable NOTIFY tableChanged)
	Q_PROPERTY (QString name READ name WRITE setName NOTIFY nameChanged)
	Q_PROPERTY (int port READ port WRITE setPort NOTIFY portChanged)

	Q_PROPERTY (QString ipT READ ipT WRITE setIpT NOTIFY ipTChanged)
	Q_PROPERTY (QString maskT READ maskT WRITE setMaskT NOTIFY maskTChanged)
	Q_PROPERTY (QString gatewayT READ gatewayT WRITE setGatewayT NOTIFY gatewayTChanged)

	Q_PROPERTY (Neighbor * neighbor1 READ neighbor1 WRITE setNeighbor1 NOTIFY neighbor1Changed)
	Q_PROPERTY (Neighbor * neighbor2 READ neighbor2 WRITE setNeighbor2 NOTIFY neighbor2Changed)
	Q_PROPERTY (Neighbor * neighbor3 READ neighbor3 WRITE setNeighbor3 NOTIFY neighbor3Changed)
	Q_PROPERTY (Neighbor * neighbor4 READ neighbor4 WRITE setNeighbor4 NOTIFY neighbor4Changed)

public:
	explicit Server (QObject *parent = nullptr);
	~Server ();

	QString address () const;
	QHostAddress ip () const;
	QHostAddress mask () const;
	QHostAddress gateway () const;
	QList <Neighbor *> table () const;
	QString name () const;
	int port () const;

	Q_INVOKABLE QString toJSON ();
	Q_INVOKABLE bool fromJSON (const QString &json);

	Q_INVOKABLE void broadcast ();

	QString ipT () const;
	QString maskT () const;
	QString gatewayT () const;

	Neighbor* neighbor1 ();
	Neighbor* neighbor2 ();
	Neighbor* neighbor3 ();
	Neighbor* neighbor4 ();

signals:

	void addressChanged (QString address);
	void ipChanged (QHostAddress ip);
	void maskChanged (QHostAddress mask);
	void gatewayChanged (QHostAddress gateway);
	void tableChanged (QList <Neighbor *> table);
	void nameChanged (QString name);
	void portChanged (int port);

	void ipTChanged (QString ipT);

	void maskTChanged (QString maskT);

	void gatewayTChanged (QString gatewayT);

	void neighbor1Changed (Neighbor *neighbor1);
	void neighbor2Changed (Neighbor *neighbor2);
	void neighbor3Changed (Neighbor *neighbor3);
	void neighbor4Changed (Neighbor *neighbor4);

	void neighborAdded (Neighbor *neighbor);

public slots:
	void setAddress (const QString &address);
	void setIp (const QHostAddress &ip);
	void setMask (const QHostAddress &mask);
	void setGateway (const QHostAddress &gateway);
	void setTable (const QList <Neighbor *> &table);
	void setName (const QString &name);
	void setPort (int port);

	void setIpT (QString ipT);
	void setMaskT (QString maskT);
	void setGatewayT (QString gatewayT);

	void setNeighbor1 (Neighbor *neighbor1);
	void setNeighbor2 (Neighbor *neighbor2);
	void setNeighbor3 (Neighbor *neighbor3);
	void setNeighbor4 (Neighbor *neighbor4);

private:
	qhttp::server::QHttpServer *server = nullptr;
	QString m_address;
	QHostAddress m_ip		= QHostAddress ("192.168.0.1");
	QHostAddress m_mask		= QHostAddress ("255.255.255.0");
	QHostAddress m_gateway	= QHostAddress ("192.168.0.1");
	QList <Neighbor *> m_table;
	QString m_name;
	int m_port = 0;

	void processPing (qhttp::server::QHttpResponse *socket, QJsonObject &obj);
	void processRip (qhttp::server::QHttpResponse *res, QJsonObject &obj);
	QString m_ipT;
	QString m_maskT;
	QString m_gatewayT;
	Neighbor *m_neighbor1;
	Neighbor *m_neighbor2;
	Neighbor *m_neighbor3;
	Neighbor *m_neighbor4;
	Neighbor n1, n2, n3, n4;
};

#endif // SERVER_H
