import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

import Cpp 1.0

Flickable {
	contentWidth: columb.width
	contentHeight: columb.height
	interactive: true

	property CppServer server: CppServer {
		onNeighborAdded: {
			neww.createObject(columb, {
								network: neighbor.networkT,
								mask: neighbor.maskT,
								nextrouter: neighbor.addressT
							  });
		}
	}

	Component {
		id: neww;

		Neww {}
	}

	Timer {
		interval: 15000;
		repeat: true;
		running: true

		onTriggered: server.broadcast();
	}

	Column {

		id: columb
		width: win.width

		onHeightChanged: console.log(height)

		ItemDelegate {
			width: parent.width
			text: "Server " + server.address;
		}

		ItemDelegate {
			width: parent.width
			text: "IP";

			TextField {
				anchors.right: parent.right
				anchors.verticalCenter: parent.verticalCenter
				onTextChanged: server.ipT = text
				Component.onCompleted: text = server.ipT
			}
		}

		ItemDelegate {
			width: parent.width
			text: "Mask";

			TextField {
				anchors.right: parent.right
				anchors.verticalCenter: parent.verticalCenter
				onTextChanged: server.maskT = text
				Component.onCompleted: text = server.maskT
			}
		}

		ItemDelegate {
			width: parent.width
			text: "Gateway";

			TextField {
				anchors.right: parent.right
				anchors.verticalCenter: parent.verticalCenter
				onTextChanged: server.gatewayT = text
				Component.onCompleted: text = server.gatewayT
			}
		}

		Neightbor {
			neightbor: server.neighbor1;
		}

		Neightbor {
			neightbor: server.neighbor2;
		}

		Neightbor {
			neightbor: server.neighbor3;
		}

		Neightbor {
			neightbor: server.neighbor4;
		}
	}
}
