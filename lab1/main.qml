import QtQuick 2.9
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

import Cpp 1.0

ApplicationWindow {
	id: win
    visible: true
    width: 640
    height: 480
    title: qsTr("Rip router")

	Material.theme: Material.Light
	Material.accent: Material.Blue
	Material.primary: Material.Teal

	ToolBar {
		id: toolbar
		width: parent.width

		RowLayout {
			anchors.fill: parent;

			Label {
				text: "Rip router";
			}

			Item {
				Layout.fillWidth: true;
			}

			ToolSeparator { }

			ToolButton {
				property int counter: 0;

				text: "Add";

				onClicked: {
					tab_button.createObject(tab_bar, {text: String(++counter)});
					server_comp.createObject(swipe_view, {});
				}
			}
		}
	}

	Component {
		id: server_comp;

		Server {
			clip: true;
		}
	}

	Component {
		id: tab_button;

		TabButton { }
	}

	TabBar {
		id: tab_bar
		currentIndex: swipe_view.currentIndex
		anchors {
			top: toolbar.bottom
			left: parent.left
			right: parent.right
		}
	}

	SwipeView {
		id: swipe_view
		currentIndex: tab_bar.currentIndex
		anchors {
			top: tab_bar.bottom
			left: parent.left
			right: parent.right
			bottom: parent.bottom
		}
	}
}
