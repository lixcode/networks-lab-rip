#ifndef HEIGHBOR_H
#define HEIGHBOR_H

#define QHTTP_HAS_CLIENT

#include <QObject>
#include <QHostAddress>
#include <QJsonObject>
#include <QAbstractSocket>
#include <QTcpSocket>
#include <qhttpclient.hpp>

class Server;

class Neighbor : public QObject
{
	Q_OBJECT

	Q_PROPERTY (QHostAddress network READ network WRITE setNetwork NOTIFY networkChanged)   //
	Q_PROPERTY (QHostAddress mask READ mask WRITE setMask NOTIFY maskChanged)               //
	Q_PROPERTY (QHostAddress address READ address WRITE setAddress NOTIFY addressChanged)   //
	Q_PROPERTY (QHostAddress ip READ ip WRITE setIp NOTIFY ipChanged)                       //
	Q_PROPERTY (QString port READ port WRITE setPort NOTIFY portChanged)
	Q_PROPERTY (int distance READ distance WRITE setDistance NOTIFY distanceChanged)
	Q_PROPERTY (bool activePort READ activePort WRITE setActivePort NOTIFY activePortChanged)
	Q_PROPERTY (int ipPort READ ipPort WRITE setIpPort NOTIFY ipPortChanged)
	Q_PROPERTY (int pingTime READ pingTime WRITE setPingTime NOTIFY pingTimeChanged)
	Q_PROPERTY (bool isHost READ isHost WRITE setIsHost NOTIFY isHostChanged)

	Q_PROPERTY (QString networkT READ networkT WRITE setNetworkT NOTIFY networkTChanged)
	Q_PROPERTY (QString maskT READ maskT WRITE setMaskT NOTIFY maskTChanged)
	Q_PROPERTY (QString addressT READ addressT WRITE setAddressT NOTIFY addressTChanged)
	Q_PROPERTY (QString ipT READ ipT WRITE setIpT NOTIFY ipTChanged)

public:
	explicit Neighbor (QObject *parent = nullptr);
	~Neighbor () override;

	QHostAddress network () const;
	QHostAddress mask () const;
	QHostAddress address () const;
	QHostAddress ip () const;
	QString port () const;
	int distance () const;

	bool activePort () const;

	Q_INVOKABLE QJsonObject toJSON ();
	Q_INVOKABLE bool fromJSON (const QJsonObject &json);
	Q_INVOKABLE void ping (const QString &localhost);
	Q_INVOKABLE void sendTable (const QJsonArray &table, Server *server);

	int ipPort () const;
	int pingTime () const;

	QString networkT () const;
	QString maskT () const;
	QString addressT () const;
	QString ipT () const;

	bool isHost() const;

signals:
	void networkChanged (QHostAddress network);
	void maskChanged (QHostAddress mask);
	void addressChanged (QHostAddress address);
	void ipChanged (QHostAddress ip);
	void portChanged (QString port);
	void distanceChanged (int distance);
	void activePortChanged (bool activePort);
	void ipPortChanged (int ipPort);
	void pingTimeChanged (int pingTime);

	void networkTChanged (QString networkT);
	void maskTChanged (QString maskT);
	void addressTChanged (QString addressT);
	void ipTChanged (QString ipT);

	void isHostChanged(bool isHost);

public slots:
	void setNetwork (const QHostAddress &network);
	void setMask (const QHostAddress &mask);
	void setAddress (const QHostAddress &address);
	void setIp (const QHostAddress &ip);
	void setPort (const QString &port);
	void setDistance (int distance);
	void setActivePort (bool activePort);

	void setIpPort (int ipPort);
	void setPingTime (int pingTime);

	void setNetworkT (QString networkT);
	void setMaskT (QString maskT);
	void setAddressT (QString addressT);
	void setIpT (QString ipT);

	void setIsHost(bool isHost);

private:
	QHostAddress m_network	= QHostAddress ("192.168.0.0");
	QHostAddress m_mask		= QHostAddress ("255.255.255.0");
	QHostAddress m_address	= QHostAddress ("0.0.0.0");
	QHostAddress m_ip		= QHostAddress ("127.0.0.1");
	QString m_port			= "0";
	int m_distance			= 0;
	bool m_activePort			= false;

	qhttp::client::QHttpClient *socket;
	qhttp::client::QHttpClient *ripSocket;
	QString localhost;
	int m_ipPort	= 11223;
	int m_pingTime	= -1;

	QString m_networkT;
	QString m_maskT;
	QString m_addressT;
	QString m_ipT;
	bool m_isHost;
};

#endif // HEIGHBOR_H
