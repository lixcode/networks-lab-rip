#include "server.h"

#include <QAbstractSocket>
#include <QDateTime>
#include <QDateTime>
#include <QDebug>
#include <QJsonArray>
#include <QJsonDocument>
#include <QSettings>
#include <qhttpclientrequest.hpp>


Server::Server (QObject *parent) : QObject (parent) {
//	for (int i = 0; i < 4; i++) {
//		m_table.append (new Neighbor () );
//	}
	n1.setActivePort(false);
	n2.setActivePort(false);
	n3.setActivePort(false);
	n4.setActivePort(false);
	m_table.append (neighbor1 () );
	m_table.append (neighbor2 () );
	m_table.append (neighbor3 () );
	m_table.append (neighbor4 () );

	server = new qhttp::server::QHttpServer ();

	server->listen (
		QHostAddress::Any,
		0,
		[this] (qhttp::server::QHttpRequest *req, qhttp::server::QHttpResponse *res) {
		req->collectData ();

		req->onEnd ( [this, req, res] () {
			const QByteArray &ba	= req->collectedData ();
			QJsonDocument doc		= QJsonDocument::fromJson (ba);
			QJsonObject obj			= doc.object ();
			QString type			= obj ["type"].toString ();

			qDebug () << "server content" << ba.size ();
			qDebug ("%s", QString::fromUtf8 (ba).toLocal8Bit ().data () );

			if (type == "ping") {
				this->processPing (res, obj);
			}
			else if (type == "rip") {
				this->processRip (res, obj);
			}
			else {
				QByteArray bao = QString (R"({"type":"error","message":"Bad request!"})").toUtf8 ();

				res->addHeader (QString ("Content-Length").toUtf8 (), QString::number (bao.size () ).toLatin1 () );
				res->addHeader (QString ("content-type").toLatin1 (), QString ("text/json; charset=UTF-8").toLatin1 () );
				res->setStatusCode (qhttp::ESTATUS_BAD_REQUEST);
				res->end (bao);
				qDebug () << "server unknown type" << type;
			}
		});
	});

	if (server->isListening () ) {
		setPort (server->tcpServer ()->serverPort () );
		setAddress (server->tcpServer ()->serverAddress ().toString () + ":" + QString::number (m_port) );
//		m_table [0]->ping (QStringLiteral ("1.2.3.4") );
//		m_table [0]->setIp (QHostAddress ("127.0.0.1") );
//		m_table [0]->setIpPort (m_port);
//		m_table [0]->setActivePort (true);
//		broadcast ();
	}
	else {
		qDebug () << server->tcpServer ()->errorString ();
	}
}

Server::~Server () {
	for (Neighbor *n : m_table) {
		delete n;
	}

	if (server != nullptr) {
		server->stopListening ();
	}
}

QString Server::address () const {
	return m_address;
}

QHostAddress Server::ip () const {
	return m_ip;
}

QHostAddress Server::mask () const {
	return m_mask;
}

QHostAddress Server::gateway () const {
	return m_gateway;
}

QList <Neighbor *> Server::table () const {
	return m_table;
}

QString Server::name () const {
	return m_name;
}

int Server::port () const {
	return m_port;
}

QString Server::toJSON () {
	QJsonDocument	doc;
	QJsonObject		obj;
	QJsonArray		arr;
	QString			str;

	for (int i = 0; i < 4; i++) {
		arr.append (m_table [i]->toJSON () );
	}

	obj.insert ("ip", m_ip.toString () );
	obj.insert ("mask", m_mask.toString () );
	obj.insert ("gateway", m_gateway.toString () );
	obj.insert ("port", m_port);
	obj.insert ("table", arr);

	doc.setObject (obj);
	str = QString::fromUtf8 (doc.toJson () );

	qDebug () << str;
	return str;
}

bool Server::fromJSON (const QString &json) {
	QJsonParseError err;
	QJsonDocument	doc = QJsonDocument::fromJson (json.toUtf8 (), &err);
	QJsonObject		obj = doc.object ();
	QJsonArray		arr = obj ["table"].toArray ();

	setIp (QHostAddress (obj ["ip"].toString () ) );
	setMask (QHostAddress (obj ["mask"].toString () ) );
	setGateway (QHostAddress (obj ["gateway"].toString () ) );
	setPort (obj ["port"].toInt () );

	for (int i = 0; i < 4; i++) {
		m_table [i]->fromJSON (arr [i].toObject () );
	}

	return err.error == QJsonParseError::NoError;
}

void Server::broadcast () {
	QJsonArray arr;

	qDebug () << "100";

	for (Neighbor *neightbor : m_table) {
		if (neightbor->activePort () ) {
			arr.push_back (neightbor->toJSON () );
			qDebug () << "1";
		}
	}

	QJsonObject obj;

	obj.insert ("type", "rip");
	obj.insert ("version", "1.0");
	obj.insert ("from", m_ip.toString () );
	obj.insert ("table", arr);


	for (Neighbor *neightbor : m_table) {
		if (neightbor->activePort () ) {
			auto *obj1 = new QJsonObject (obj);
			obj1->insert ("to", neightbor->address ().toString () );

			auto *client = new qhttp::client::QHttpClient ();
			connect (client, &qhttp::client::QHttpClient::disconnected, client, &qhttp::client::QHttpClient::deleteLater);

			qDebug () << neightbor->ip ().toString () << QString::number (neightbor->ipPort () );



			qDebug () << client->request (
				qhttp::EHTTP_GET,
				"http://" + neightbor->ip ().toString () + ":" + QString::number (neightbor->ipPort () ),
				[obj1] (qhttp::client::QHttpRequest *req) {
				QJsonDocument doc;
				QByteArray ba;

				doc.setObject (*obj1);
				ba = doc.toJson ();

				req->addHeader (QString ("Content-Length").toUtf8 (), QString::number (ba.size () ).toLatin1 () );
				req->end (ba);

				qDebug () << "connected";
			},
				[] (qhttp::client::QHttpResponse *) {
				qDebug () << "reply";
			});
		}
	}
}

QString Server::ipT () const {
	return m_ip.toString ();
}

QString Server::maskT () const {
	return m_mask.toString ();
}

QString Server::gatewayT () const {
	return m_gateway.toString ();
}

Neighbor * Server::neighbor1 () {
	return &n1;
}

Neighbor * Server::neighbor2 () {
	return &n2;
}

Neighbor * Server::neighbor3 () {
	return &n3;
}

Neighbor * Server::neighbor4 () {
	return &n4;
}

void Server::setAddress (const QString &address) {
	if (m_address == address) {
		return;
	}

	m_address = address;
	emit addressChanged (m_address);
}

void Server::setIp (const QHostAddress &ip) {
	if (m_ip == ip) {
		return;
	}

	m_ip = ip;
	setIpT (m_ip.toString () );
	emit ipChanged (m_ip);
}



void Server::setMask (const QHostAddress &mask) {
	if (m_mask == mask) {
		return;
	}

	m_mask = mask;
	setMaskT (m_mask.toString () );
	emit maskChanged (m_mask);
}

void Server::setGateway (const QHostAddress &gateway) {
	if (m_gateway == gateway) {
		return;
	}

	m_gateway = gateway;
	setGatewayT (m_gateway.toString () );
	emit gatewayChanged (m_gateway);
}

void Server::setTable (const QList <Neighbor *> &table) {
	if (m_table == table) {
		return;
	}

	m_table = table;
	emit tableChanged (m_table);
}

void Server::setName (const QString &name) {
	if (m_name == name) {
		return;
	}

	m_name = name;
	emit nameChanged (m_name);

	if (m_table.isEmpty () ) {
		QString json = QSettings ().value ("server_" + name, toJSON () ).toString ();

		fromJSON (json);
	}
}

void Server::setPort (int port) {
	if (m_port == port) {
		return;
	}

	m_port = port;
	emit portChanged (m_port);
}

void Server::setIpT (QString ipT) {
	if (m_ip.toString () == ipT) {
		return;
	}

	m_ipT = ipT;
	setIp (QHostAddress (m_ipT) );
	emit ipTChanged (m_ipT);
}

void Server::setMaskT (QString maskT) {
	if (m_mask.toString () == maskT) {
		return;
	}

	m_maskT = maskT;
	setMask (QHostAddress (m_maskT) );
	emit maskTChanged (m_maskT);
}

void Server::setGatewayT (QString gatewayT) {
	if (m_gateway.toString () == gatewayT) {
		return;
	}

	m_gatewayT = gatewayT;
	setGateway (QHostAddress (m_gatewayT) );
	emit gatewayTChanged (m_gatewayT);
}

void Server::setNeighbor1 (Neighbor *neighbor1) {
	if (m_neighbor1 == neighbor1) {
		return;
	}

	m_neighbor1 = neighbor1;
	emit neighbor1Changed (m_neighbor1);
}

void Server::setNeighbor2 (Neighbor *neighbor2) {
	if (m_neighbor2 == neighbor2) {
		return;
	}

	m_neighbor2 = neighbor2;
	emit neighbor2Changed (m_neighbor2);
}

void Server::setNeighbor3 (Neighbor *neighbor3) {
	if (m_neighbor3 == neighbor3) {
		return;
	}

	m_neighbor3 = neighbor3;
	emit neighbor3Changed (m_neighbor3);
}

void Server::setNeighbor4 (Neighbor *neighbor4) {
	if (m_neighbor4 == neighbor4) {
		return;
	}

	m_neighbor4 = neighbor4;
	emit neighbor4Changed (m_neighbor4);
}

void Server::processPing (qhttp::server::QHttpResponse *res, QJsonObject &obj) {
	QJsonDocument	doc;
	QJsonObject		obj1;

	obj1.insert ("type", "ping-response");
	obj1.insert (QStringLiteral ("from"), m_ip.toString () );
	obj1.insert ("to", obj ["from"]);
	obj1.insert ("time", obj ["time"]);

	doc.setObject (obj1);
	res->setStatusCode (qhttp::ESTATUS_OK);

	QByteArray ba = doc.toJson ();

	res->addHeader (QString ("Content-Length").toLatin1 (), QString::number (ba.size () ).toLatin1 () );
	res->end (doc.toJson () );
}

void Server::processRip (qhttp::server::QHttpResponse *res, QJsonObject &obj) {
	QByteArray bao;

	if (obj ["version"].toString () == "1.0") {
		QJsonDocument	doc;
		QJsonObject		obj1;

		obj1.insert ("type", "rip-response");
		obj1.insert (QStringLiteral ("from"), m_ip.toString () );
		obj1.insert ("to", obj ["from"]);

		doc.setObject (obj1);
		res->setStatusCode (qhttp::ESTATUS_OK);

		for (QJsonValueRef ref : obj ["table"].toArray () ) {
			auto	o = ref.toObject ();

			auto	network			= QHostAddress (o ["network"].toString () );
			bool	already_exists	= false;

			for (Neighbor *n : m_table) {
				if (n->network () == network) {
					already_exists = true;
				}
			}

			if (!already_exists) {
				auto *neww = new Neighbor ();

				neww->setActivePort (true);
				neww->setNetwork (network);
				neww->setMask (QHostAddress (o ["mask"].toString () ) );
				neww->setAddress (QHostAddress (obj ["from"].toString () ) );
				neww->setDistance (o ["distance"].toInt () + 1);
				neww->setIp (QHostAddress (o ["ip"].toString () ) );
				neww->setIpPort (o ["ipport"].toInt () );

				m_table.push_back (neww);
				emit neighborAdded(neww);
			}
		}
	}
	else {
		bao = QString (R"({"type":"error","message":"RIP version not supported!"})").toUtf8 ();
		res->setStatusCode (qhttp::ESTATUS_BAD_REQUEST);
	}

	res->addHeader (QString ("Content-Length").toUtf8 (), QString::number (bao.size () ).toLatin1 () );
	res->addHeader (QString ("content-type").toLatin1 (), QString ("text/json; charset=UTF-8").toLatin1 () );
	res->end (bao);
}
