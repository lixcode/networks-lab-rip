import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

import Cpp 1.0

Column {
	id: content
	width: win.width

	property alias network: network_item.text
	property alias mask: mask_item.text
	property alias nextrouter: next_router.text

	ItemDelegate {
		width: parent.width
		text: "New Neightbor";
	}

	ItemDelegate {
		width: parent.width
		text: "Network";

		TextField {
			id: network_item;
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter
			readOnly: true
		}
	}

	ItemDelegate {
		width: parent.width
		text: "Mask";

		TextField {
			id: mask_item
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter
			readOnly: true
		}
	}

	ItemDelegate {
		width: parent.width
		text: "Next Router";

		TextField {
			id: next_router
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter
			readOnly: true
		}
	}
}
