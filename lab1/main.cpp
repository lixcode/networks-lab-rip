#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QtQuickControls2/QQuickStyle>

#include <cpp/server.h>

int main (int argc, char *argv []) {
	QQuickStyle::setStyle("Material");
	QCoreApplication::setAttribute (Qt::AA_EnableHighDpiScaling);

	QGuiApplication app (argc, argv);

	qmlRegisterType <Server> ("Cpp", 1, 0, "CppServer");
	qmlRegisterType <Neighbor> ("Cpp", 1, 0, "CppNeighbor");

	QQmlApplicationEngine engine;
	engine.load (QUrl (QStringLiteral ("qrc:/main.qml") ) );
	if (engine.rootObjects ().isEmpty () ) {
		return -1;
	}

	return app.exec ();
}
