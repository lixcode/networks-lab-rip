import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.1
import QtQuick.Controls.Material 2.1

import Cpp 1.0

Column {
	property CppNeighbor neightbor: null
	id: content
	width: win.width

	Timer {
		id: timer;
		interval: 1500;
		repeat: true;
		running: true

		onTriggered: neightbor.ping(server.ipT);
	}

	Connections {
		target: neightbor;
		onActivePortChanged: {
			if (neightbor.activePort) {
				timer.start();
			}
			else {
				timer.stop();
			}
		}
	}

	SwitchDelegate {
		width: parent.width
		text: "Neightbor";

		position: 0;
		onPositionChanged: neightbor.activePort = (position == 1);
	}

	Component.onCompleted:  {
		console.log(server.table.length)
	}

	ItemDelegate {
		width: parent.width
		text: "Network";

		TextField {
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter
			onTextChanged: neightbor.networkT = text
			Component.onCompleted: text = neightbor.networkT
		}
	}

	ItemDelegate {
		width: parent.width
		text: "Mask";

		TextField {
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter
			onTextChanged: neightbor.maskT = text
			Component.onCompleted: text = neightbor.maskT
		}
	}

	ItemDelegate {
		width: parent.width
		text: "Next Router";

		TextField {
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter
			onTextChanged: neightbor.addressT = text
			Component.onCompleted: text = neightbor.addressT
		}
	}

	ItemDelegate {
		width: parent.width
		text: "IP";

		TextField {
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter
			onTextChanged: neightbor.ipT = text
			Component.onCompleted: text = neightbor.ipT
		}
	}

	ItemDelegate {
		width: parent.width
		text: "Port";

		TextField {
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter
			onTextChanged: neightbor.ipPort = Number(text)
			Component.onCompleted: text = neightbor.ipPort
		}
	}

	ItemDelegate {
		width: parent.width
		text: "Ping time";

		Label {
			text: neightbor.pingTime
			anchors.right: parent.right
			anchors.verticalCenter: parent.verticalCenter
		}
	}
}
