#include "neighbor.h"
#include "server.h"

#include <QDateTime>
#include <QJsonDocument>
#include <QJsonArray>
#include <qhttpclientrequest.hpp>
#include <qhttpclientresponse.hpp>

Neighbor::Neighbor (QObject *parent) : QObject (parent) {
	socket = new qhttp::client::QHttpClient ();
}

Neighbor::~Neighbor () {
	delete socket;
}

QHostAddress Neighbor::network () const {
	return m_network;
}

QHostAddress Neighbor::mask () const {
	return m_mask;
}

QHostAddress Neighbor::address () const {
	return m_address;
}

QHostAddress Neighbor::ip () const {
	return m_ip;
}

QString Neighbor::port () const {
	return m_port;
}

int Neighbor::distance () const {
	return m_distance;
}

bool Neighbor::activePort () const {
	return m_activePort;
}

QJsonObject Neighbor::toJSON () {
	QJsonObject json;

	json.insert ("network", QJsonValue (m_network.toString () ) );
	json.insert ("mask", QJsonValue (m_mask.toString () ) );
	json.insert ("address", QJsonValue (m_address.toString () ) );
	json.insert ("port", QJsonValue (m_port) );
	json.insert ("ip", QJsonValue (m_ip.toString () ) );
	json.insert ("ipport", QJsonValue (m_ipPort) );
	json.insert ("distance", QJsonValue (m_distance) );
	json.insert ("active", QJsonValue (m_activePort) );

	return json;
}

bool Neighbor::fromJSON (const QJsonObject &json) {
	setNetwork (QHostAddress (json ["network"].toString () ) );
	setMask (QHostAddress (json ["mask"].toString () ) );
	setAddress (QHostAddress (json ["address"].toString () ) );
	setPort (json ["port"].toString () );
	setIp (QHostAddress (json ["ip"].toString () ) );
	setIpPort (json ["ipport"].toInt () );
	setDistance (json ["distance"].toInt () );
	setActivePort (false);

	return true;
}

void Neighbor::ping (const QString &localhost) {
	if (!m_activePort) return;
	qDebug() << m_activePort << "mactive";

	this->localhost = localhost;

	if (socket->isOpen () ) {
		socket->killConnection ();
	}

	socket->request (
		qhttp::EHTTP_GET,
		"http://" + m_ip.toString () + ":" + QString::number (m_ipPort),
		[this] (qhttp::client::QHttpRequest *req) {
		QJsonDocument   doc;
		QJsonObject     obj;

		obj.insert ("type", "ping");
		obj.insert ("to", m_address.toString () );
		obj.insert ("from", this->localhost);
		obj.insert ("time", QDateTime::currentMSecsSinceEpoch () );

		doc.setObject (obj);

		QByteArray ba = doc.toJson ();

		req->addHeader (QString ("Content-Length").toUtf8 (), QString::number (ba.size () ).toLatin1 () );
		req->end (ba);
	},
		[this] (qhttp::client::QHttpResponse *res) {
		if (res->status () ==  qhttp::ESTATUS_OK) {

			res->collectData ();
			res->onEnd ( [res, this] () {
				QJsonDocument   doc = QJsonDocument::fromJson (res->collectedData () );
				QJsonObject     obj = doc.object ();

				if (obj ["type"].toString () == "ping-response") {
					setPingTime (int ( double ( QDateTime::currentMSecsSinceEpoch () ) - obj ["time"].toDouble () ) );
					this->setActivePort (true);
				}
				else {
					this->setActivePort (false);
				}
			});
		}
		else {
			this->setActivePort (false);
		}
	});

	socket->setTimeOut (1000);
}

void Neighbor::sendTable (const QJsonArray &table, Server *server) {
	if (ripSocket->isOpen () ) {
		ripSocket->killConnection ();
	}

	socket->request (
		qhttp::EHTTP_GET,
		"http://" + m_ip.toString () + ":" + QString::number (m_ipPort),
		[table, server, this] (qhttp::client::QHttpRequest *req) {
		QJsonDocument   doc;
		QJsonObject     obj;

		obj.insert ("type", "rip");
		obj.insert ("version", "1.0");
		obj.insert ("to", m_address.toString () );
		obj.insert ("from", server->ip ().toString () );
		obj.insert ("table", table);

		doc.setObject (obj);

		QByteArray ba = doc.toJson ();

		req->addHeader (QString ("Content-Length").toUtf8 (), QString::number (ba.size () ).toLatin1 () );
		req->end (ba);
	}, nullptr);
}

int Neighbor::ipPort () const {
	return m_ipPort;
}

int Neighbor::pingTime () const {
	return m_pingTime;
}

QString Neighbor::networkT () const {
	return m_network.toString ();
}

QString Neighbor::maskT () const {
	return m_mask.toString ();
}

QString Neighbor::addressT () const {
	return m_address.toString ();
}

QString Neighbor::ipT () const {
	return m_ip.toString ();
}

bool Neighbor::isHost () const {
	return m_isHost;
}

void Neighbor::setNetwork (const QHostAddress &network) {
	if (m_network == network) {
		return;
	}

	m_network = network;
	setNetworkT (network.toString () );
	emit networkChanged (m_network);
}

void Neighbor::setMask (const QHostAddress &mask) {
	if (m_mask == mask) {
		return;
	}

	m_mask = mask;
	setMaskT (mask.toString () );
	emit maskChanged (m_mask);
}

void Neighbor::setAddress (const QHostAddress &address) {
	if (m_address == address) {
		return;
	}

	m_address = address;
	setAddressT (address.toString () );
	emit addressChanged (m_address);
}

void Neighbor::setIp (const QHostAddress &ip) {
	if (m_ip == ip) {
		return;
	}

	m_ip = ip;
	setIpT (ip.toString () );
	emit ipChanged (m_ip);
}

void Neighbor::setPort (const QString &port) {
	if (m_port == port) {
		return;
	}

	m_port = port;
	emit portChanged (m_port);
}

void Neighbor::setDistance (int distance) {
	if (m_distance == distance) {
		return;
	}

	m_distance = distance;
	emit distanceChanged (m_distance);
}

void Neighbor::setActivePort (bool active) {
	if (m_activePort == active) {
		return;
	}

	m_activePort = active;
	emit activePortChanged (m_activePort);
}

void Neighbor::setIpPort (int ipPort) {
	if (m_ipPort == ipPort) {
		return;
	}

	m_ipPort = ipPort;
	emit ipPortChanged (m_ipPort);
}

void Neighbor::setPingTime (int pingTime) {
	if (m_pingTime == pingTime) {
		return;
	}

	m_pingTime = pingTime;
	emit pingTimeChanged (m_pingTime);
}

void Neighbor::setNetworkT (QString networkT) {
	if (m_network.toString () == networkT) {
		return;
	}

	m_networkT = networkT;
	setNetwork (QHostAddress (networkT) );
	emit networkTChanged (m_networkT);
}

void Neighbor::setMaskT (QString maskT) {
	if (m_mask.toString () == maskT) {
		return;
	}

	m_maskT = maskT;
	setNetwork (QHostAddress (maskT) );
	emit maskTChanged (m_maskT);
}

void Neighbor::setAddressT (QString addressT) {
	if (m_address.toString () == addressT) {
		return;
	}

	m_addressT = addressT;
	setAddress (QHostAddress (addressT) );
	emit addressTChanged (m_addressT);
}

void Neighbor::setIpT (QString ipT) {
	if (m_ip.toString () == ipT) {
		return;
	}

	m_ipT = ipT;
	setIp (QHostAddress (ipT) );
	emit ipTChanged (m_ipT);
}

void Neighbor::setIsHost (bool isHost) {
	if (m_isHost == isHost) {
		return;
	}

	m_isHost = isHost;
	emit isHostChanged (m_isHost);
}
